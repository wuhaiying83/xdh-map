import Measure from './src/measure'

Measure.install = function (Vue) {
  Vue.component(Measure.name, Measure)
}
export default Measure